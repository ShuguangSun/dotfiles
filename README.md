# dotfiles

fc's dotfiles, using [dotbot](https://github.com/anishathalye/dotbot).

for a list of programs, see [programs.md](programs.md).

## setup

the below assumes an [arch-based distro](https://wiki.archlinux.org/index.php/Arch-based_distributions)

prereqs: [paru](https://github.com/morganamilo/paru)/[yay](https://github.com/Jguer/yay), git, ssh

```shell
# install stuff
> paru -S zsh antigen p7zip tmux neofetch neovim packer node-lua-fmt prettier
> paru -S bat bat-extras-git fd lsd fzf git-delta-bin ripgrep bottom-bin
> paru -S ttf-hackgen nerd-fonts-hack nerd-fonts-noto noto-fonts-cjk noto-fonts-emoji-blob
> paru -S alacritty mpv imv zathura zathura-pdf-poppler mcomix-gtk3-git anki
> paru -S sway swaywsr swayidle swaylock-effects-git brightnessctl mako slurp grim swappy wl-clipboard kanshi waybar wofi autotiling flashfocus-git
# for waybar volume control, you may also need pulseaudio, pavucontrol

# if necessary, change default shell to zsh
> chsh -s $(command -v zsh)

# clone repo with submodules
> git clone --recurse-submodules -j8 https://codeberg.org/usashiki/dotfiles.git ~/.dotfiles
# if you forget the recurse-submodule flag: git submodule update --init --recursive
> cd ~/.dotfiles
> ./install

> nvim --headless +PackerSync +qa # or open nvim and run :PackerSync

# antigen should install plugins on shell restart
```

to run updates

```shell
# update submodules
> ./install # runs git submodule sync --recursive && git submodule update --init --recursive

> paru # defaults to paru -Syu
> antigen update # antigen selfupdate is through paru

> nvim --headless +PackerSync +qa # or open nvim and run :PackerSync
```

note: to update vim-plug, do not run `:PlugUpgrade`. instead, update the git submodule.
