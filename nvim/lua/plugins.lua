return require("packer").startup(
  function()
    -- packer: plugin management
    -- https://github.com/wbthomason/packer.nvim
    use {
      "wbthomason/packer.nvim",
      config = function()
        -- Run when this file is changed
        vim.cmd(
          [[
            augroup packer_user_config
              autocmd!
              autocmd BufWritePost plugins.lua source <afile> | PackerCompile
            augroup end
          ]]
        )
      end
    }

    -- sensible defaults
    -- https://github.com/tpope/vim-sensible
    use "tpope/vim-sensible"

    -- toggle between relative and absolute line numbers
    -- https://github.com/jeffkreeftmeijer/vim-numbertoggle
    use "jeffkreeftmeijer/vim-numbertoggle"

    -- show indentation guides
    -- https://github.com/lukas-reineke/indent-blankline.nvim
    use {
      "lukas-reineke/indent-blankline.nvim",
      config = function()
        require("indent_blankline").setup {
          char = "▏",
          show_first_indent_level = false,
          space_char_blanklie = " ",
          filetype_exclude = {"dashboard", "help"},
          buftype_exclude = {"terminal"}
        }

        vim.opt.listchars = {
          tab = "▸▸",
          trail = "·"
        }
      end
    }

    -- cursorline: highlight words and lines on the cursor
    -- https://github.com/yamatsum/nvim-cursorline
    use "yamatsum/nvim-cursorline"

    -- show git decorations
    -- https://github.com/lewis6991/gitsigns.nvim
    use {
      "lewis6991/gitsigns.nvim",
      requires = {"nvim-lua/plenary.nvim"},
      config = function()
        require("gitsigns").setup()
      end
    }

    -- statusline
    -- https://github.com/hoob3rt/lualine.nvim
    use {
      "hoob3rt/lualine.nvim",
      requires = {"kyazdani42/nvim-web-devicons", opt = true},
      config = function()
        require("lualine").setup {
          sections = {
            lualine_b = {"branch", "diff"}
          }
        }
      end
    }

    -- file explorer
    -- https://github.com/kyazdani42/nvim-tree.lua
    use {
      "kyazdani42/nvim-tree.lua",
      requires = "kyazdani42/nvim-web-devicons",
      config = function()
        require("nvim-tree").setup()

        vim.cmd(
          [[
            nnoremap <C-n> :NvimTreeToggle<CR>
            nnoremap <leader>r :NvimTreeRefresh<CR>
            nnoremap <leader>n :NvimTreeFindFile<CR>
          ]]
        )
      end
    }

    -- fuzzy file search
    -- https://github.com/nvim-telescope/telescope.nvim
    use {
      "nvim-telescope/telescope.nvim",
      requires = {"nvim-lua/plenary.nvim"},
      config = function()
        local actions = require("telescope.actions")

        require("telescope").setup {
          defaults = {
            sorting_strategy = "ascending",
            layout_strategy = "vertical",
            layout_config = {
              vertical = {mirror = true}
            },
            mappings = {
              i = {
                ["<esc>"] = actions.close
              }
            }
          }
        }

        -- shortcuts
        vim.cmd(
          [[
            nnoremap <leader>ff <cmd>Telescope find_files<cr>
            nnoremap <leader>fg <cmd>Telescope live_grep<cr>
            nnoremap <leader>fb <cmd>Telescope buffers<cr>
            nnoremap <leader>fh <cmd>Telescope help_tags<cr>
          ]]
        )

        -- run find_files on start
        vim.cmd("autocmd VimEnter * Telescope find_files")
      end
    }

    -- syntax highlighting: TSInstall <lang>
    -- https://github.com/nvim-treesitter/nvim-treesitter
    use {
      "nvim-treesitter/nvim-treesitter",
      run = ":TSUpdate",
      config = function()
        require("nvim-treesitter.configs").setup {
          highlight = {enable = true},
          indent = {enable = true}
        }
      end
    }

    -- formatter: requires prettier, lua-fmt
    -- https://github.com/mhartington/formatter.nvim
    use {
      "mhartington/formatter.nvim",
      config = function()
        require("formatter").setup {
          filetype = {
            json = {
              function()
                return {
                  exe = "prettier",
                  args = {
                    "--stdin-filepath",
                    vim.fn.fnameescape(vim.api.nvim_buf_get_name(0)),
                    "--single-quote"
                  },
                  stdin = true
                }
              end
            },
            lua = {
              function()
                return {
                  exe = "luafmt",
                  args = {"--indent-count", 2, "--stdin"},
                  stdin = true
                }
              end
            },
            markdown = {
              function()
                return {
                  exe = "prettier",
                  args = {
                    "--stdin-filepath",
                    vim.fn.fnameescape(vim.api.nvim_buf_get_name(0)),
                    "--single-quote"
                  },
                  stdin = true
                }
              end
            },
            yaml = {
              function()
                return {
                  exe = "prettier",
                  args = {
                    "--stdin-filepath",
                    vim.fn.fnameescape(vim.api.nvim_buf_get_name(0)),
                    "--single-quote"
                  },
                  stdin = true
                }
              end
            }
          }
        }

        -- Run on file save
        vim.api.nvim_exec(
          [[
            augroup FormatAutogroup
              autocmd!
              autocmd BufWritePost *.json,*.lua,*.md,*.yaml,*.yml FormatWrite
            augroup END
          ]],
          true
        )
      end
    }

    -- comment lines
    -- https://github.com/b3nj5m1n/kommentary
    use {
      "b3nj5m1n/kommentary",
      config = function()
        require("kommentary.config").use_extended_mappings()
      end
    }

    -- move lines
    -- https://github.com/fedepujol/move.nvim
    use {
      "fedepujol/move.nvim",
      config = function()
        require("move")
        vim.cmd(
          [[
            nnoremap <silent> <A-j> :MoveLine(1)<CR>
            nnoremap <silent> <A-k> :MoveLine(-1)<CR>
            vnoremap <silent> <A-j> :MoveBlock(1)<CR>
            vnoremap <silent> <A-k> :MoveBlock(-1)<CR>
            nnoremap <silent> <A-l> :MoveHChar(1)<CR>
            nnoremap <silent> <A-h> :MoveHChar(-1)<CR>
            vnoremap <silent> <A-l> :MoveHBlock(1)<CR>
            vnoremap <silent> <A-h> :MoveHBlock(-1)<CR>
          ]]
        )
      end
    }

    -- highlight todos, etc
    -- https://github.com/folke/todo-comments.nvim
    use {
      "folke/todo-comments.nvim",
      requires = "nvim-lua/plenary.nvim",
      config = function()
        require("todo-comments").setup()
      end
    }

    -- create folders when saving files
    -- https://github.com/jghauser/mkdir.nvim
    use {
      "jghauser/mkdir.nvim",
      config = function()
        require("mkdir")
      end
    }

    -- markdown helper
    -- https://github.com/sidofc/mkdx
    use {
      "sidofc/mkdx",
      config = function()
        vim.cmd(
          [[
            let g:mkdx#settings = { 'highlight': { 'enable': 1 },
                                  \ 'enter': { 'shift': 1 },
                                  \ 'checkbox': { 'toggles': [' ', 'x'] },
                                  \ 'tokens': { 'strike': '~~', 'italic': '_' },
                                  \ 'toc': { 'text': 'Table of Contents', 'update_on_write': 1 } }
          ]]
        )
      end
    }

    -- markdown preview
    -- https://github.com/iamcco/markdown-preview.nvim
    use {
      "iamcco/markdown-preview.nvim",
      config = "vim.call('mkdp#util#install')"
    }

    -- todo.txt
    -- https://github.com/freitass/todo.txt-vim
    use {
      "freitass/todo.txt-vim",
      config = function()
        vim.cmd(
          [[
            let g:todo_done_filename = 'done.txt'
            filetype plugin on
            let maplocalleader=" "
          ]]
        )
      end
    }
  end
)
