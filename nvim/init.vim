" import plugins via packer
lua require('plugins')

" set leader to space
let mapleader = " "

" Use 256 gui colors
set t_Co=256
if exists('+termguicolors')
  let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

" Enable line numbers
set number
set relativenumber

" Case insensitive searching
set ignorecase
set smartcase

" Input tabs as spaces
set expandtab shiftwidth=2

" Turn on regex
set magic

" Turn off line wrapping
set nowrap

" Enable mouse mode
set mouse=a

" Highlight stuff
hi Whitespace ctermfg=DarkGrey guifg=DarkGrey
hi EndOfBuffer ctermfg=DarkGrey guifg=DarkGrey

" Use the system clipboard
set clipboard=unnamedplus

" Prevent typos around wq
cnoreabbrev <expr> W ((getcmdtype() is# ':' && getcmdline() is# 'W')?('w'):('W'))
cnoreabbrev <expr> Q ((getcmdtype() is# ':' && getcmdline() is# 'Q')?('q'):('Q'))
cnoreabbrev <expr> Wq ((getcmdtype() is# ':' && getcmdline() is# 'Wq')?('wq'):('Wq'))
cnoreabbrev <expr> wQ ((getcmdtype() is# ':' && getcmdline() is# 'wQ')?('wq'):('wQ'))
cnoreabbrev <expr> WQ ((getcmdtype() is# ':' && getcmdline() is# 'WQ')?('wq'):('WQ'))
